package dominio;

import java.io.Serializable;

public class Alimento implements Serializable {
    private String nombre;
    private String tipo;
    private int porcion;
    private float[] nutrientes;
    
    public String getNombre(){
       return this.nombre;
    } 
   
    public void setNombre(String aNombre){
       this.nombre = aNombre;
    }
    
    public String getTipo(){
       return this.tipo;
    } 
   
    public void setTipo(String aTipo){
       this.tipo = aTipo;
    }
   
    public int getPorcion(){
       return this.porcion;
    }
   
    public void setPorcion(int aPorcion){
       this.porcion = aPorcion;       
    }
   
    public float[] getNutrientes(){
       return this.nutrientes;
    } 
   
    public void setNutrientes(float[] aNutrientes){
       this.nutrientes = aNutrientes;
    }

   @Override
    public boolean equals(Object obj){ //
        boolean equals = false;
        Alimento unAlimento;
        if(obj instanceof Alimento){
            unAlimento = (Alimento)obj;
            equals = this.nombre.equalsIgnoreCase(unAlimento.getNombre());
        }
        return equals;
    }
}
