package dominio;

import java.io.Serializable;

public class Consulta implements Serializable{
    private Usuario user;
    private String pregunta;
    private String respuesta;
    private Profesional pro;

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Profesional getPro() {
        return pro;
    }

    public void setPro(Profesional pro) {
        this.pro = pro;
    }
    
    public void enviarRespuesta() {
        this.user.agregarRespuesta(this);
    }
    
}


