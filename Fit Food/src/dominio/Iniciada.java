package dominio;

import java.io.Serializable;
import javax.swing.ImageIcon;

public class Iniciada implements Serializable {
    
    private Usuario usuario;
    private Profesional profesional;
    private ImageIcon foto;
    private boolean esPro;

    public Usuario getUsuario(){
        return usuario;
    }
    
    public void setUsuario(Usuario usu){
        this.usuario = usu;
    }   
    
    public Profesional getProfesional() {
        return profesional;
    }

    public void setProfesional(Profesional prof) {
        this.profesional = prof;
    }

    public ImageIcon getFoto() {
        return foto;
    }

    public void setFoto(ImageIcon foto) {
        this.foto = foto;
    }
    
    public boolean isEsPro() {
        return esPro;
    }

    public void setEsPro(boolean esPro) {
        this.esPro = esPro;
    }
    
}