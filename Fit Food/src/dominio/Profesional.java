package dominio;

import java.io.Serializable;
import javax.swing.ImageIcon;

public class Profesional implements Serializable {
    private String nombre;
    private String apellido;
    private String titulo;
    private int fecha_D;
    private int fecha_M;
    private int fecha_A;
    private int fechaG_D;
    private int fechaG_M;
    private int fechaG_A;
    private String pais_T;
    private ImageIcon foto;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getFecha_D() {
        return fecha_D;
    }

    public void setFecha_D(int fecha_D) {
        this.fecha_D = fecha_D;
    }

    public int getFecha_M() {
        return fecha_M;
    }

    public void setFecha_M(int fecha_M) {
        this.fecha_M = fecha_M;
    }

    public int getFecha_A() {
        return fecha_A;
    }

    public void setFecha_A(int fecha_A) {
        this.fecha_A = fecha_A;
    }

    public int getFechaG_D() {
        return fechaG_D;
    }

    public void setFechaG_D(int fechaG_D) {
        this.fechaG_D = fechaG_D;
    }

    public int getFechaG_M() {
        return fechaG_M;
    }

    public void setFechaG_M(int fechaG_M) {
        this.fechaG_M = fechaG_M;
    }

    public int getFechaG_A() {
        return fechaG_A;
    }

    public void setFechaG_A(int fechaG_A) {
        this.fechaG_A = fechaG_A;
    }

    public String getPais_T() {
        return pais_T;
    }

    public void setPais_T(String pais_T) {
        this.pais_T = pais_T;
    }

    public ImageIcon getFoto() {
        return foto;
    }

    public void setFoto(ImageIcon foto) {
        this.foto = foto;
    }

    @Override
    public boolean equals(Object obj){ //
        boolean equals = false;
        Profesional unProfesional;
        if(obj instanceof Profesional){
            unProfesional = (Profesional)obj;
            
            equals = (this.nombre + " " + this.apellido).equalsIgnoreCase(unProfesional.getNombre() + " " + unProfesional.getApellido());
        }
        return equals;
    }

}

