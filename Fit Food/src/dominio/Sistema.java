package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;

public class Sistema implements Serializable{

    private ArrayList<Usuario> listaUsuarios;
    private ArrayList<Profesional> listaProfesionales;
    private ArrayList<Alimento> listaAlimentos;
    private ArrayList<Usuario> listaPlanes;
    public ArrayList<Consulta> listaConsultas;
    
    
    public Sistema() {
        this.listaUsuarios = new ArrayList<>();
        this.listaProfesionales = new ArrayList<>();
        this.listaAlimentos = new ArrayList<>();
        this.listaPlanes = new ArrayList<>();
        this.listaConsultas = new ArrayList<>();
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }
    
    public void setListaUsuarios(ArrayList<Usuario> lista){
        this.listaUsuarios=lista;
    }
    
    public ArrayList<Profesional> getListaProfesionales() {
        return listaProfesionales;
    }
    
    public void setListaProfesionales(ArrayList<Profesional> lista){
        this.listaProfesionales=lista;
    }

    public ArrayList<Alimento> getListAlimentos() {
        return listaAlimentos;
    }
    
    public void setListaAlimentos(ArrayList<Alimento> lista){
        this.listaAlimentos=lista;
    }

    public ArrayList<Usuario> getListaPlanes() {
        return listaPlanes;
    }

    public void setListaPlanes(ArrayList<Usuario> listaPlanes) {
        this.listaPlanes = listaPlanes;
    }
    
    public ArrayList<Consulta> getListaConsultas(){
        return listaConsultas;
    }
    public void setListaConsultas(ArrayList<Consulta> consultas){
       this.listaConsultas = consultas;
    }
    
    public void agregarUsuario(String nombre, String apellido, String nacionalidad, int dia, int mes, int anio, boolean[] problemas, String otros, ImageIcon foto) {
        Usuario nuevo = new Usuario();
        nuevo.setNombre(nombre);
        nuevo.setApellido(apellido);
        nuevo.setNacionalidad(nacionalidad);
        nuevo.setFecha_D(dia);
        nuevo.setFecha_M(mes);
        nuevo.setFecha_A(anio);
        nuevo.setListaPreferencias(problemas);
        nuevo.setOtrasPreferencias(otros);
        nuevo.setFoto(foto);
        this.listaUsuarios.add(nuevo);
    }
    
    public boolean existeUsuario(String nombre, String apellido) {
        Usuario usuarioAux = new Usuario();
        usuarioAux.setNombre(nombre);
        usuarioAux.setApellido(apellido);
        return this.listaUsuarios.contains(usuarioAux);
    }
 

    public void agregarProfesional(String nombre, String apellido, String titulo, int diaN, int mesN, int anioN, int diaG, int mesG, int anioG, String pais, ImageIcon foto){
        Profesional nuevo = new Profesional();
        nuevo.setNombre(nombre);
        nuevo.setApellido(apellido);
        nuevo.setTitulo(titulo);
        nuevo.setFecha_D(diaN);
        nuevo.setFecha_M(mesN);
        nuevo.setFecha_A(anioN);
        nuevo.setFechaG_A(anioG);
        nuevo.setFechaG_D(diaG);
        nuevo.setFechaG_M(mesG);
        nuevo.setFoto(foto); 
        this.listaProfesionales.add(nuevo);
    }
    
    public boolean existeProfesional(String nombre, String apellido) {
        Profesional profesionalAux = new Profesional();
        profesionalAux.setNombre(nombre);
        profesionalAux.setApellido(apellido);
        return this.listaProfesionales.contains(profesionalAux);
    }
    
    public void agregarAlimento(String nombre, String tipo, int porcion, float[] nutrientes){
        Alimento nuevo = new Alimento();
        nuevo.setNombre(nombre);
        nuevo.setTipo(tipo);
        nuevo.setNutrientes(nutrientes);
        nuevo.setPorcion(porcion);
        this.listaAlimentos.add(nuevo);
    }

    public Alimento getAlimento(String s) {
        Alimento alimento = null;
        for(int i = 0; i<this.listaAlimentos.size(); i++){
            if (this.listaAlimentos.get(i).getNombre().equals(s)) {
                alimento = this.listaAlimentos.get(i);
            }
        }
        return alimento;
    }
    
    public boolean existeAlimento(String s) {
        boolean existe = false;
        for(int i = 0; i<this.listaAlimentos.size() && !existe; i++){
            if (this.listaAlimentos.get(i).getNombre().equals(s)) {
                existe = true;
            }
        }
        return existe;
    }

    public void agregarConsulta(Consulta consu){
       this.listaConsultas.add(consu);
    }
    
    public void eliminarConsulta(Consulta consulta) {
        this.listaConsultas.remove(consulta);
    }
    
    public ArrayList<Alimento> deStringAalimentos (ArrayList<String> alimento){
        ArrayList<Alimento> nuevo = new ArrayList();
        for(int i = 0; i< alimento.size();i++){
            nuevo.set(i, this.getAlimento(alimento.get(i)));
        }
        return nuevo;
    }
    
    public ArrayList<String> deListaArrayList(List<String> lista){
        ArrayList<String> nuevo = new ArrayList();
        for(int i =0 ; i< lista.size(); i++){
            nuevo.set(i, lista.get(i));
        }
        return nuevo;
    }
    
    
    public String[] deAlimentoAString(ArrayList<Alimento> array){
        String [] todo = new String[array.size()];
        for(int i = 0 ;i < array.size();i++){
            todo [i] = array.get(i).getNombre();
        }
        return todo;
    }
    
}





