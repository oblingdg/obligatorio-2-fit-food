package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.ImageIcon;


public class Usuario implements Serializable{ 
    private String nombre;
    private String apellido;
    private int fecha_D;
    private int fecha_M;
    private int fecha_A;
    private String nacionalidad;
    private boolean[] listaPreferencias;
    private String otrasPreferencias;
    private ImageIcon foto;
    private ArrayList<Alimento> alimentosIngeridos;
    private ArrayList<Consulta> respuestas;
    private ArrayList<ArrayList<Alimento>[]> planes;
    
    public Usuario(){
        this.alimentosIngeridos = new ArrayList<>();
        this.respuestas = new ArrayList<>();
        this.planes = new ArrayList<ArrayList<Alimento>[]>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getFecha_D() {
        return fecha_D;
    }

    public void setFecha_D(int fecha_D) {
        this.fecha_D = fecha_D;
    }

    public int getFecha_M() {
        return fecha_M;
    }

    public void setFecha_M(int fecha_M) {
        this.fecha_M = fecha_M;
    }

    public int getFecha_A() {
        return fecha_A;
    }

    public void setFecha_A(int fecha_A) {
        this.fecha_A = fecha_A;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public boolean[] getListaPreferencias() {
        return listaPreferencias;
    }

    public void setListaPreferencias(boolean[] listaPreferencias) {
        this.listaPreferencias = listaPreferencias;
    }

    public String getOtrasPreferencias() {
        return otrasPreferencias;
    }

    public void setOtrasPreferencias(String otrasPreferencias) {
        this.otrasPreferencias = otrasPreferencias;
    }

    public ImageIcon getFoto() {
        return foto;
    }

    public void setFoto(ImageIcon foto) {
        this.foto = foto;
    }

    public ArrayList<Alimento> getAlimentosIngeridos() {
        return alimentosIngeridos;
    }

    public void setAlimentosIngeridos(ArrayList<Alimento> alimentosIngeridos) {
        this.alimentosIngeridos = alimentosIngeridos;
    }

    public ArrayList<Consulta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(ArrayList<Consulta> respuestas) {
        this.respuestas = respuestas;
    }
    
    public ArrayList<ArrayList<Alimento>[]> getPlanes() {
        return planes;
    }

    public void setPlanes(ArrayList<ArrayList<Alimento>[]> planes) {
        this.planes = planes;
    }
    
    public void agregarRespuesta(Consulta consulta) {
        this.respuestas.add(consulta);
    }
    
    public void agregarPlan( ArrayList<Alimento>[] alimentos){
        getPlanes().add(alimentos);
    }

    @Override
    public boolean equals(Object obj){ //
        boolean equals = false;
        Usuario unUsuario;
        if(obj instanceof Usuario){
            unUsuario = (Usuario)obj;
            equals = (this.nombre + " " + this.apellido).equalsIgnoreCase(unUsuario.getNombre() + " " + unUsuario.getApellido());
        }
        return equals;
    }
    
}