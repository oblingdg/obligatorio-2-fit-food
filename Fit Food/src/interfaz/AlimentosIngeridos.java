package interfaz;

import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class AlimentosIngeridos extends javax.swing.JFrame {

    Sistema sistema;
    Usuario user;
    MenuPrincipalUsuario menuPrincipalU;
    
    public AlimentosIngeridos(Sistema sis, Usuario user, MenuPrincipalUsuario menuPrincipalU) {
        initComponents();
        this.sistema = sis;
        this.user = user;
        this.menuPrincipalU = menuPrincipalU;
        comboTipoActionPerformed(new java.awt.event.ActionEvent(new Object(), 0, "")); //para que muestre al principio.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLinea = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        btnIngresar = new javax.swing.JButton();
        comboTipo = new javax.swing.JComboBox<>();
        lTipo = new javax.swing.JLabel();
        lAlimento = new javax.swing.JLabel();
        comboAlimento = new javax.swing.JComboBox<>();
        lPorciones = new javax.swing.JLabel();
        spinnerPorciones = new javax.swing.JSpinner();
        lAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLinea.setBackground(new java.awt.Color(196, 57, 58));
        jLinea.setForeground(new java.awt.Color(196, 57, 58));
        jLinea.setOpaque(true);
        jPanel1.add(jLinea, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        btnIngresar.setForeground(new java.awt.Color(196, 57, 58));
        btnIngresar.setText("Ingresar");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        jPanel1.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 410, 150, 40));

        comboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Derivados de la leche", "Carnes y huevo", "Legumbres y frutos secos", "Hortalizas y verduras", "Frutas", "Cereales" }));
        comboTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTipoActionPerformed(evt);
            }
        });
        jPanel1.add(comboTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 210, -1));

        lTipo.setText("Tipo");
        jPanel1.add(lTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 200, -1, -1));

        lAlimento.setText("Alimento");
        jPanel1.add(lAlimento, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 250, -1, -1));

        jPanel1.add(comboAlimento, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 250, 150, -1));

        lPorciones.setText("Porciones");
        jPanel1.add(lPorciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 310, -1, -1));

        spinnerPorciones.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        jPanel1.add(spinnerPorciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 310, 60, -1));

        lAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        lAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lAtrasMouseClicked(evt);
            }
        });
        jPanel1.add(lAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lAtrasMouseClicked
        this.menuPrincipalU.setLocationRelativeTo(this);
        this.menuPrincipalU.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lAtrasMouseClicked

    private void comboTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTipoActionPerformed
       this.comboAlimento.removeAllItems();
       ArrayList<Alimento> alimentos = this.sistema.getListAlimentos();
       for(int i = 0; i<alimentos.size(); i++){
           Alimento alimento = alimentos.get(i);
           if (alimento.getTipo().equals(this.comboTipo.getSelectedItem())) {
               this.comboAlimento.addItem(alimento.getNombre());
           }
       }
    }//GEN-LAST:event_comboTipoActionPerformed

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        if(this.comboAlimento.getItemCount() == 0){
            JOptionPane.showMessageDialog(null, "Debe haber un alimento seleccionado.", "Error al seleccionar alimento", 0);  
        }
        else{
            Alimento a = this.sistema.getAlimento(this.comboAlimento.getSelectedItem().toString());
            this.user.getAlimentosIngeridos().add(a);
            this.menuPrincipalU.setLocationRelativeTo(this);
            this.menuPrincipalU.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnIngresarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIngresar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboAlimento;
    private javax.swing.JComboBox<String> comboTipo;
    private javax.swing.JSeparator jLinea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lAlimento;
    private javax.swing.JLabel lAtras;
    private javax.swing.JLabel lPorciones;
    private javax.swing.JLabel lTipo;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JSpinner spinnerPorciones;
    // End of variables declaration//GEN-END:variables
}
