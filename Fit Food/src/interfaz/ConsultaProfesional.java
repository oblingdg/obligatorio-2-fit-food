package interfaz;

import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

public class ConsultaProfesional extends javax.swing.JFrame {

    Sistema sistema;
    Profesional pro;
    MenuPrincipalProfesional menuPrincipalP;
    Consulta consulta;
    
    public ConsultaProfesional(Sistema sis, Profesional pro, MenuPrincipalProfesional menuProfesional) {
        initComponents();
        this.sistema = sis;
        this.pro = pro;
        this.menuPrincipalP = menuProfesional;
        for(int i = 0; i < this.sistema.getListaConsultas().size() ; i++){
            this.comboConsultas.addItem(Integer.toString(i));
        }
        comboConsultasActionPerformed(new java.awt.event.ActionEvent(new Object(), 0, ""));                                                                                       
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLinea = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        btnIngresar = new javax.swing.JButton();
        lAtras = new javax.swing.JLabel();
        lSubtitulo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taConsulta = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taRespuesta = new javax.swing.JTextArea();
        comboConsultas = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLinea.setBackground(new java.awt.Color(196, 57, 58));
        jLinea.setForeground(new java.awt.Color(196, 57, 58));
        jLinea.setOpaque(true);
        jPanel1.add(jLinea, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        btnIngresar.setForeground(new java.awt.Color(196, 57, 58));
        btnIngresar.setText("Enviar");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        jPanel1.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 580, 150, 40));

        lAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        lAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lAtrasMouseClicked(evt);
            }
        });
        jPanel1.add(lAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        lSubtitulo.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        lSubtitulo.setForeground(new java.awt.Color(255, 255, 255));
        lSubtitulo.setText("Consulta de un usuario");
        jPanel1.add(lSubtitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, -1, -1));

        jLabel3.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Consulta:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));

        taConsulta.setEditable(false);
        taConsulta.setColumns(20);
        taConsulta.setRows(5);
        jScrollPane1.setViewportView(taConsulta);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 170, 360, 170));

        jLabel4.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Respuesta:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, -1, -1));

        taRespuesta.setColumns(20);
        taRespuesta.setRows(5);
        jScrollPane2.setViewportView(taRespuesta);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 390, 360, 170));

        comboConsultas.setEditable(true);
        comboConsultas.setMaximumRowCount(100);
        comboConsultas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboConsultasActionPerformed(evt);
            }
        });
        jPanel1.add(comboConsultas, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 100, 70, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 30, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        if (this.taRespuesta.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Debe haber texto en la respuesta.", "Error al enviar respuesta", 0);
        }
        else{
            this.consulta.setRespuesta(this.taRespuesta.getText());
            this.consulta.setPro(this.pro);
            this.consulta.enviarRespuesta();
            this.sistema.eliminarConsulta(this.consulta);   
            this.menuPrincipalP.setLocationRelativeTo(this);
            this.menuPrincipalP.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnIngresarActionPerformed

    private void lAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lAtrasMouseClicked
        this.menuPrincipalP.setLocationRelativeTo(this);
        this.menuPrincipalP.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lAtrasMouseClicked

    private void comboConsultasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboConsultasActionPerformed
        this.consulta = this.sistema.getListaConsultas().get(this.comboConsultas.getSelectedIndex());
        this.taConsulta.setText(this.consulta.getPregunta());
        this.taRespuesta.setText(null);
    }//GEN-LAST:event_comboConsultasActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIngresar;
    private javax.swing.JComboBox<String> comboConsultas;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jLinea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lAtras;
    private javax.swing.JLabel lSubtitulo;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JTextArea taConsulta;
    private javax.swing.JTextArea taRespuesta;
    // End of variables declaration//GEN-END:variables
}
