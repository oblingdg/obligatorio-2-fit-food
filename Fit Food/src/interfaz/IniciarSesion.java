package interfaz;
import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

public class IniciarSesion extends javax.swing.JFrame {

    Sistema sistema;
    VentanaDeInicio ventanaInicio;
    
    public IniciarSesion(Sistema sis, VentanaDeInicio ventanaInicio) {
        initComponents();
        this.sistema = sis;
        this.ventanaInicio = ventanaInicio;
        this.comboUsuarios.removeAllItems();
        this.comboProfesionales.removeAllItems();
        for(int i = 0; i < this.sistema.getListaUsuarios().size() ; i++){
            Usuario user = this.sistema.getListaUsuarios().get(i);
            this.comboUsuarios.addItem(user.getNombre() + " " + user.getApellido());
        }
        for(int i = 0 ; i < this.sistema.getListaProfesionales().size();i++){
            Profesional prof = this.sistema.getListaProfesionales().get(i);
            this.comboProfesionales.addItem(prof.getNombre() + " " + prof.getApellido());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        comboUsuarios = new javax.swing.JComboBox<>();
        comboProfesionales = new javax.swing.JComboBox<>();
        btnIniciarSesion = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        lSubtitulo = new javax.swing.JLabel();
        radioUsuario = new javax.swing.JRadioButton();
        radioProfesional = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(100, 100, 520, 700));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(520, 700));
        setMinimumSize(new java.awt.Dimension(520, 700));
        setPreferredSize(new java.awt.Dimension(520, 700));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setMaximumSize(new java.awt.Dimension(520, 670));
        jPanel1.setMinimumSize(new java.awt.Dimension(520, 670));
        jPanel1.setPreferredSize(new java.awt.Dimension(520, 670));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.add(comboUsuarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 191, -1));

        jPanel1.add(comboProfesionales, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 360, 190, -1));

        btnIniciarSesion.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion.setText("INICIAR SESION");
        btnIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesionActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 520, 170, -1));

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        lSubtitulo.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        lSubtitulo.setForeground(new java.awt.Color(255, 255, 255));
        lSubtitulo.setText("INICIAR SESION COMO:");
        jPanel1.add(lSubtitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, -1, -1));

        btnGroup.add(radioUsuario);
        radioUsuario.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        radioUsuario.setForeground(new java.awt.Color(255, 255, 255));
        radioUsuario.setSelected(true);
        radioUsuario.setText("Usuario");
        radioUsuario.setOpaque(false);
        jPanel1.add(radioUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, -1, -1));

        btnGroup.add(radioProfesional);
        radioProfesional.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        radioProfesional.setForeground(new java.awt.Color(255, 255, 255));
        radioProfesional.setText("Profesional");
        radioProfesional.setOpaque(false);
        jPanel1.add(radioProfesional, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 670));
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesionActionPerformed
        if(this.radioUsuario.isSelected()){
            if(this.sistema.getListaUsuarios().isEmpty()){
                JOptionPane.showMessageDialog(null, "Debe haber al menos 1 usuario ingresado en el sistema.", "Error al iniciar sesion", 0);
            }

            else{
                Iniciada sesion = new Iniciada();
                Usuario user = this.sistema.getListaUsuarios().get(this.comboUsuarios.getSelectedIndex());
                sesion.setUsuario(user);
                sesion.setEsPro(false);
                sesion.setFoto(user.getFoto());
                //abrir menu principal para usuario
                MenuPrincipalUsuario menuPrincipalU = new MenuPrincipalUsuario(this.sistema, ventanaInicio, sesion);
                menuPrincipalU.setLocationRelativeTo(this);
                menuPrincipalU.setVisible(true);
                this.dispose();
            }
        }
        else{ //this.radioProfesional.isSelected()
            if(this.sistema.getListaProfesionales().isEmpty()){
                JOptionPane.showMessageDialog(null, "Debe haber al menos 1 profesional ingresado en el sistema.", "Error al iniciar sesion", 0);
            }
            else{
                Iniciada sesion = new Iniciada();
                Profesional prof = this.sistema.getListaProfesionales().get(this.comboProfesionales.getSelectedIndex());
                sesion.setProfesional(prof);
                sesion.setEsPro(true);
                sesion.setFoto(prof.getFoto());
                //abrir menu principal para profesional
                this.setVisible(false);
                MenuPrincipalProfesional menuPrincipalP = new MenuPrincipalProfesional(this.sistema, ventanaInicio, sesion);
                menuPrincipalP.setLocationRelativeTo(this);
                menuPrincipalP.setVisible(true);
            }
        }
    }//GEN-LAST:event_btnIniciarSesionActionPerformed

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        this.ventanaInicio.setLocationRelativeTo(this);
        this.ventanaInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLabel2MouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGroup;
    private javax.swing.JButton btnIniciarSesion;
    private javax.swing.JComboBox<String> comboProfesionales;
    private javax.swing.JComboBox<String> comboUsuarios;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lSubtitulo;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JRadioButton radioProfesional;
    private javax.swing.JRadioButton radioUsuario;
    // End of variables declaration//GEN-END:variables
}
