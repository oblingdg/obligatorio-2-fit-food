package interfaz;
import dominio.*;
import java.io.*;

public class Inicio {
    
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {     
        
        Sistema sistema = new Sistema();
        try {
            FileInputStream archivo=new FileInputStream("persistencia.fitfood");
            BufferedInputStream buf=new BufferedInputStream(archivo);
            ObjectInputStream obj= new ObjectInputStream(buf);
            sistema = (Sistema)(obj.readObject());
            obj.close();
        } catch (Exception e) {

        }        
        VentanaDeInicio v = new VentanaDeInicio(sistema);
        v.setVisible(true);
    }
    
}

        
 







