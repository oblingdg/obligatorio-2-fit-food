package interfaz;

import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

public class MenuPrincipalProfesional extends javax.swing.JFrame {

    Sistema sistema;
    VentanaDeInicio ventanaInicio;
    Iniciada sesion;
    
    public MenuPrincipalProfesional(Sistema sis, VentanaDeInicio ventanaInicio, Iniciada sesion) {
        initComponents();
        this.sistema = sis;
        this.ventanaInicio = ventanaInicio;
        this.sesion = sesion;
        this.lFoto.setIcon(this.sesion.getFoto());
        this.lNombre.setText(this.sesion.getProfesional().getNombre() + " " + this.sesion.getProfesional().getApellido());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnIniciarSesion = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnIniciarSesion1 = new javax.swing.JButton();
        btnIniciarSesion3 = new javax.swing.JButton();
        btnCerrarSesion = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lNombre = new javax.swing.JLabel();
        lFoto = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnIniciarSesion.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion.setText("Registro de alimentos");
        btnIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesionActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 260, 200, 70));

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        jLabel6.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("FIT FOOD");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        jLabel7.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Menu Principal");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 80, -1, -1));

        btnIniciarSesion1.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion1.setText("Consulta directa");
        btnIniciarSesion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesion1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciarSesion1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, 200, 70));

        btnIniciarSesion3.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion3.setText("Plan de alimentacion");
        btnIniciarSesion3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesion3ActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciarSesion3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 420, 200, 70));

        btnCerrarSesion.setForeground(new java.awt.Color(196, 57, 58));
        btnCerrarSesion.setText("Cerrar Sesion");
        btnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSesionActionPerformed(evt);
            }
        });
        jPanel1.add(btnCerrarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 600, 200, -1));

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        lNombre.setText("Nombre");

        lFoto.setMaximumSize(new java.awt.Dimension(50, 50));
        lFoto.setMinimumSize(new java.awt.Dimension(50, 50));
        lFoto.setName(""); // NOI18N
        lFoto.setPreferredSize(new java.awt.Dimension(50, 50));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(lFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lNombre)
                .addContainerGap(185, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 310, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSesionActionPerformed
        this.ventanaInicio.setLocationRelativeTo(this);
        this.ventanaInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCerrarSesionActionPerformed

    private void btnIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesionActionPerformed
        RegistroAlimento regAlimento = new RegistroAlimento(this.sistema, this, true);
        regAlimento.setLocationRelativeTo(this);
        regAlimento.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnIniciarSesionActionPerformed

    private void btnIniciarSesion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesion1ActionPerformed
    if(this.sistema.getListaConsultas().isEmpty()){
        JOptionPane.showMessageDialog(null, "Debe haber al menos una consulta ingresada en el sistema.", "Error de consulta directa", 0);
    }
    else{
        this.setVisible(false);
        ConsultaProfesional consultaP = new ConsultaProfesional(this.sistema, this.sesion.getProfesional(), this);
        consultaP.setLocationRelativeTo(this);
        consultaP.setVisible(true);
    }
        
    }//GEN-LAST:event_btnIniciarSesion1ActionPerformed

    private void btnIniciarSesion3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesion3ActionPerformed
        if(this.sistema.getListaPlanes().isEmpty() || this.sistema.getListAlimentos().isEmpty()){
        JOptionPane.showMessageDialog(null, "Debe haber al menos un alimento y un plan pedido por usuarios ingresado en el sistema.", "Error de planes de alimentacion", 0);
    }
    else{
        this.setVisible(false);
        PlanProfesional planP = new PlanProfesional(this.sistema, this);
        planP.setLocationRelativeTo(this);
        planP.setVisible(true);
    }
    }//GEN-LAST:event_btnIniciarSesion3ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrarSesion;
    private javax.swing.JButton btnIniciarSesion;
    private javax.swing.JButton btnIniciarSesion1;
    private javax.swing.JButton btnIniciarSesion3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lFoto;
    private javax.swing.JLabel lNombre;
    // End of variables declaration//GEN-END:variables
}
