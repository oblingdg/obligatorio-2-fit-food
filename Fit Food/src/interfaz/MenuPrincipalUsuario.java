package interfaz;
import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class MenuPrincipalUsuario extends javax.swing.JFrame {

    Sistema sistema;
    VentanaDeInicio ventanaInicio;
    Iniciada sesion;
    
    public MenuPrincipalUsuario(Sistema sis, VentanaDeInicio ventanaInicio, Iniciada sesion) {
        initComponents();
        this.sistema = sis;
        this.ventanaInicio = ventanaInicio;
        this.sesion = sesion;
        this.lFoto.setIcon(this.sesion.getFoto());
        this.lNombre.setText(this.sesion.getUsuario().getNombre() + " " + this.sesion.getUsuario().getApellido());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnCerrarSesion = new javax.swing.JButton();
        btnAlimentosIng = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnConsultaDir = new javax.swing.JButton();
        btnRegAlimentos = new javax.swing.JButton();
        btnPlanAli = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lNombre = new javax.swing.JLabel();
        lFoto = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCerrarSesion.setForeground(new java.awt.Color(196, 57, 58));
        btnCerrarSesion.setText("Cerrar Sesion");
        btnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSesionActionPerformed(evt);
            }
        });
        jPanel1.add(btnCerrarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 600, 200, -1));

        btnAlimentosIng.setForeground(new java.awt.Color(196, 57, 58));
        btnAlimentosIng.setText("Alimentos Ingeridos");
        btnAlimentosIng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlimentosIngActionPerformed(evt);
            }
        });
        jPanel1.add(btnAlimentosIng, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 260, 200, 70));

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        jLabel6.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("FIT FOOD");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        jLabel7.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Menu Principal");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 80, -1, -1));

        btnConsultaDir.setForeground(new java.awt.Color(196, 57, 58));
        btnConsultaDir.setText("Consulta directa");
        btnConsultaDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaDirActionPerformed(evt);
            }
        });
        jPanel1.add(btnConsultaDir, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 420, 200, 70));

        btnRegAlimentos.setForeground(new java.awt.Color(196, 57, 58));
        btnRegAlimentos.setText("Registro de alimentos");
        btnRegAlimentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegAlimentosActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegAlimentos, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, 200, 70));

        btnPlanAli.setForeground(new java.awt.Color(196, 57, 58));
        btnPlanAli.setText("Plan de alimentacion");
        btnPlanAli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlanAliActionPerformed(evt);
            }
        });
        jPanel1.add(btnPlanAli, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 500, 200, 70));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        lNombre.setText("Nombre");

        lFoto.setMaximumSize(new java.awt.Dimension(50, 50));
        lFoto.setMinimumSize(new java.awt.Dimension(50, 50));
        lFoto.setName(""); // NOI18N
        lFoto.setPreferredSize(new java.awt.Dimension(50, 50));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(lFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lNombre)
                .addContainerGap(185, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 310, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSesionActionPerformed
        this.ventanaInicio.setLocationRelativeTo(this);
        this.ventanaInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCerrarSesionActionPerformed

    private void btnAlimentosIngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlimentosIngActionPerformed
        this.setVisible(false);
        AlimentosIngeridos alimentosIng = new AlimentosIngeridos(this.sistema, this.sesion.getUsuario(), this);
        alimentosIng.setLocationRelativeTo(this);
        alimentosIng.setVisible(true);
    }//GEN-LAST:event_btnAlimentosIngActionPerformed

    private void btnRegAlimentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegAlimentosActionPerformed
        this.setVisible(false);
        RegistroAlimento regAlimento = new RegistroAlimento(this.sistema, this, false);
        regAlimento.setLocationRelativeTo(this);
        regAlimento.setVisible(true);
    }//GEN-LAST:event_btnRegAlimentosActionPerformed

    private void btnConsultaDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaDirActionPerformed
        this.setVisible(false);
        ConsultaUsuario consultaU = new ConsultaUsuario(this.sistema, this.sesion.getUsuario(), this);
        consultaU.setLocationRelativeTo(this);
        consultaU.setVisible(true);
    }//GEN-LAST:event_btnConsultaDirActionPerformed

    private void btnPlanAliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlanAliActionPerformed
        this.setVisible(false);
        PlanesDeUsuarios planesU = new PlanesDeUsuarios(this.sistema, this.sesion.getUsuario(), this);
        planesU.setLocationRelativeTo(this);
        planesU.setVisible(true);
    }//GEN-LAST:event_btnPlanAliActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlimentosIng;
    private javax.swing.JButton btnCerrarSesion;
    private javax.swing.JButton btnConsultaDir;
    private javax.swing.JButton btnPlanAli;
    private javax.swing.JButton btnRegAlimentos;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lFoto;
    private javax.swing.JLabel lNombre;
    // End of variables declaration//GEN-END:variables
}
