package interfaz;

import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class PlanProfesional extends javax.swing.JFrame {

    Sistema sistema;
    Usuario seleccionado;
    ArrayList<Alimento>[] plan;
    MenuPrincipalProfesional menuPrincipalP;
    
    public PlanProfesional(Sistema sis, MenuPrincipalProfesional menuPp) {
        initComponents();
        this.sistema=sis;
        this.comboUsuarios.removeAllItems();
        plan = new ArrayList[7];
        for(int i = 0; i < 7; i++){
            plan[i] = new ArrayList<>();
        }
        for(int i = 0; i < this.sistema.getListaPlanes().size() ; i++){
            Usuario user = this.sistema.getListaPlanes().get(i);
            this.comboUsuarios.addItem(user.getNombre() + " " + user.getApellido());
            this.menuPrincipalP=menuPp;
        }        
    }
     public String[] dePreferenciaAString(boolean[] array, String otro){
        int contador = 0;
        for(int i = 0; i<array.length; i++){
            if (array[i]){
                contador++;
            }
        }
        String[] nuevo = new String[contador];
        int j = 0;
        for(int i = 0; i<array.length; i++){
            if(array[i]){
                switch (i){
                    case 0: 
                        nuevo[j] = "Vegano"; 
                        j++;
                        break;
                    case 1: 
                        nuevo[j] = "Vegetariano"; 
                        j++;
                        break;
                    case 2: 
                        nuevo[j] = "Macrobiotico"; 
                        j++;
                        break;
                    case 3: 
                        nuevo[j] = "Organico";
                        j++;
                        break;
                    case 4:
                        nuevo[j] = "Fugivoro";
                        j++;
                        break;
                    case 5: 
                        nuevo[j] = "Diabetes";
                        j++;
                        break;
                    case 6: 
                        nuevo[j] = "Celiaco";
                        j++;
                        break;
                    case 7:
                        nuevo[j] = otro;
                        j++;
                        break;
                    default:
                        break;
                }
            }
        }
        return nuevo; 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLinea1 = new javax.swing.JSeparator();
        jLinea = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        preferencias = new javax.swing.JList<>();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        consumidos = new javax.swing.JList<>();
        foto = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        apellido = new javax.swing.JLabel();
        dias = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaAlimentos = new javax.swing.JList<>();
        confirmar = new javax.swing.JButton();
        enviar = new javax.swing.JButton();
        lAtras = new javax.swing.JLabel();
        comboUsuarios = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(770, 710));
        setPreferredSize(new java.awt.Dimension(770, 710));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setMinimumSize(new java.awt.Dimension(800, 800));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLinea1.setBackground(new java.awt.Color(196, 57, 58));
        jLinea1.setForeground(new java.awt.Color(196, 57, 58));
        jLinea1.setOpaque(true);
        jPanel1.add(jLinea1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 58, 780, -1));

        jLinea.setBackground(new java.awt.Color(196, 57, 58));
        jLinea.setForeground(new java.awt.Color(196, 57, 58));
        jLinea.setOpaque(true);
        jPanel1.add(jLinea, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 65, 750, 0));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 0, -1, -1));

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Preferencias:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 130, 30));

        jScrollPane1.setViewportView(preferencias);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 140, 110, 130));

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Alimentos consumidos:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 90, 220, 40));

        jScrollPane3.setViewportView(consumidos);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 140, 110, 130));

        foto.setText("foto");
        jPanel1.add(foto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 110, 100));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Realizar plan para:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 170, 40));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Apellido:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, -1, -1));

        nombre.setText("nombre");
        jPanel1.add(nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 150, 130, -1));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Nombre:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, -1, -1));

        apellido.setText("apellido");
        jPanel1.add(apellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, 140, -1));

        dias.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" }));
        dias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diasActionPerformed(evt);
            }
        });
        jPanel1.add(dias, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 360, 160, 30));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Seleccione día:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 350, 110, 60));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Seleccione Alimentos:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 480, 250, 40));

        jScrollPane2.setViewportView(listaAlimentos);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 420, 170, 190));

        confirmar.setBackground(new java.awt.Color(255, 255, 255));
        confirmar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        confirmar.setForeground(new java.awt.Color(102, 0, 0));
        confirmar.setText("CONFIRMAR");
        confirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarActionPerformed(evt);
            }
        });
        jPanel1.add(confirmar, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 460, 160, 40));

        enviar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        enviar.setForeground(new java.awt.Color(102, 0, 0));
        enviar.setText("ENVIAR PLAN");
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });
        jPanel1.add(enviar, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 560, 150, 50));

        lAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        lAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lAtrasMouseClicked(evt);
            }
        });
        jPanel1.add(lAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        comboUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUsuariosActionPerformed(evt);
            }
        });
        jPanel1.add(comboUsuarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 100, 191, -1));

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 770, 710);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void diasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_diasActionPerformed

    private void confirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarActionPerformed
        int dia = dias.getSelectedIndex();
        plan[dia].add(sistema.getAlimento(listaAlimentos.getSelectedValue()));           
    }//GEN-LAST:event_confirmarActionPerformed

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        boolean estanTodos = true;
        for(int i = 0; i<7 ; i++){
            if(plan[i].isEmpty()){
                estanTodos=false;
            }
        }
        if(estanTodos){
            seleccionado.agregarPlan(plan);
            this.menuPrincipalP.setLocationRelativeTo(this);
            this.menuPrincipalP.setVisible(true);
            this.dispose();
        }
        else{
            JOptionPane.showMessageDialog(null, "Para ingresar un plan, todos los días deben estar completos", "Error al enviar plan", 0);
        }
    }//GEN-LAST:event_enviarActionPerformed

    private void lAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lAtrasMouseClicked
        this.menuPrincipalP.setLocationRelativeTo(this);
        this.menuPrincipalP.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lAtrasMouseClicked

    private void comboUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUsuariosActionPerformed
        int pos = comboUsuarios.getSelectedIndex();
        seleccionado = sistema.getListaPlanes().get(pos);
        nombre.setText(seleccionado.getNombre());
        apellido.setText(seleccionado.getApellido());
        foto.setIcon(seleccionado.getFoto());
        preferencias.setListData(this.dePreferenciaAString(seleccionado.getListaPreferencias(), seleccionado.getOtrasPreferencias()));
        consumidos.setListData(this.sistema.deAlimentoAString(seleccionado.getAlimentosIngeridos()));
        listaAlimentos.setListData(this.sistema.deAlimentoAString(sistema.getListAlimentos()));
    }//GEN-LAST:event_comboUsuariosActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apellido;
    private javax.swing.JComboBox<String> comboUsuarios;
    private javax.swing.JButton confirmar;
    private javax.swing.JList<String> consumidos;
    private javax.swing.JComboBox<String> dias;
    private javax.swing.JButton enviar;
    private javax.swing.JLabel foto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jLinea;
    private javax.swing.JSeparator jLinea1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lAtras;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JList<String> listaAlimentos;
    private javax.swing.JLabel nombre;
    private javax.swing.JList<String> preferencias;
    // End of variables declaration//GEN-END:variables
}
