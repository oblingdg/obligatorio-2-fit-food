package interfaz;
import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class PlanesDeUsuarios extends javax.swing.JFrame {

    Sistema sistema;
    Usuario user;
    MenuPrincipalUsuario menuPrincipalU;
    
    public PlanesDeUsuarios(Sistema s,Usuario user, MenuPrincipalUsuario menuPrincipalU) {
        initComponents();
        this.user = user;
        this.sistema = s; 
        this.menuPrincipalU = menuPrincipalU;
        for(int j=0;j<this.user.getPlanes().size() ; j++){
            plan.addItem(Integer.toString(j));
        }
    }
    

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lAtras = new javax.swing.JLabel();
        plan = new javax.swing.JComboBox<>();
        jScrollPane37 = new javax.swing.JScrollPane();
        domingo = new javax.swing.JList<>();
        jScrollPane69 = new javax.swing.JScrollPane();
        jueves = new javax.swing.JList<>();
        jScrollPane38 = new javax.swing.JScrollPane();
        miercoles = new javax.swing.JList<>();
        jScrollPane70 = new javax.swing.JScrollPane();
        sabado = new javax.swing.JList<>();
        jScrollPane39 = new javax.swing.JScrollPane();
        viernes = new javax.swing.JList<>();
        jLinea = new javax.swing.JSeparator();
        jScrollPane71 = new javax.swing.JScrollPane();
        martes = new javax.swing.JList<>();
        jScrollPane72 = new javax.swing.JScrollPane();
        lunes = new javax.swing.JList<>();
        lTitulo = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(610, 710));
        setPreferredSize(new java.awt.Dimension(610, 710));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        lAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lAtrasMouseClicked(evt);
            }
        });
        jPanel1.add(lAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        plan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                planActionPerformed(evt);
            }
        });
        jPanel1.add(plan, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 140, 110, -1));

        domingo.setBackground(new java.awt.Color(47, 68, 26));
        domingo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        domingo.setForeground(new java.awt.Color(255, 255, 255));
        domingo.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane37.setViewportView(domingo);

        jPanel1.add(jScrollPane37, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 270, 80, 210));

        jueves.setBackground(new java.awt.Color(47, 68, 26));
        jueves.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jueves.setForeground(new java.awt.Color(255, 255, 255));
        jueves.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane69.setViewportView(jueves);

        jPanel1.add(jScrollPane69, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 270, 90, 210));

        miercoles.setBackground(new java.awt.Color(47, 68, 26));
        miercoles.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        miercoles.setForeground(new java.awt.Color(255, 255, 255));
        miercoles.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane38.setViewportView(miercoles);

        jPanel1.add(jScrollPane38, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 270, 80, 210));

        sabado.setBackground(new java.awt.Color(47, 68, 26));
        sabado.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        sabado.setForeground(new java.awt.Color(255, 255, 255));
        sabado.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane70.setViewportView(sabado);

        jPanel1.add(jScrollPane70, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 270, 90, 210));

        viernes.setBackground(new java.awt.Color(47, 68, 26));
        viernes.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        viernes.setForeground(new java.awt.Color(255, 255, 255));
        viernes.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane39.setViewportView(viernes);

        jPanel1.add(jScrollPane39, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 270, 80, 210));

        jLinea.setBackground(new java.awt.Color(196, 57, 58));
        jLinea.setForeground(new java.awt.Color(196, 57, 58));
        jLinea.setOpaque(true);
        jPanel1.add(jLinea, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 63, 610, -1));

        martes.setBackground(new java.awt.Color(47, 68, 26));
        martes.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        martes.setForeground(new java.awt.Color(255, 255, 255));
        martes.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane71.setViewportView(martes);

        jPanel1.add(jScrollPane71, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 90, 210));

        lunes.setBackground(new java.awt.Color(47, 68, 26));
        lunes.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        lunes.setForeground(new java.awt.Color(255, 255, 255));
        lunes.setSelectionBackground(new java.awt.Color(153, 0, 0));
        jScrollPane72.setViewportView(lunes);

        jPanel1.add(jScrollPane72, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 80, 210));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(153, 0, 0));
        jButton1.setText("Solicitar Plan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 80, -1, -1));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Seleccionar Plan:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 140, 40));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Domingo");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 240, 100, 20));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Lunes");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 70, 20));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Martes");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 240, 70, 20));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Miércoles");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, 100, 20));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Jueves");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, 70, 20));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Viernes");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 240, 70, 20));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Sábado");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 240, 70, 20));

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 610, 710);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void planActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_planActionPerformed
        int pos = plan.getSelectedIndex();
        ArrayList<Alimento>[] plan = this.user.getPlanes().get(pos);
        lunes.setListData(this.sistema.deAlimentoAString(plan[0]));
        martes.setListData(this.sistema.deAlimentoAString(plan[1]));
        miercoles.setListData(this.sistema.deAlimentoAString(plan[2]));
        jueves.setListData(this.sistema.deAlimentoAString(plan[3]));
        viernes.setListData(this.sistema.deAlimentoAString(plan[4]));
        sabado.setListData(this.sistema.deAlimentoAString(plan[5]));
        domingo.setListData(this.sistema.deAlimentoAString(plan[6])); 
    }//GEN-LAST:event_planActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.sistema.getListaPlanes().add(user);
        JOptionPane.showMessageDialog(null, "Ha solicitado un plan.", "Plan solicitado", 1);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void lAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lAtrasMouseClicked
        this.menuPrincipalU.setLocationRelativeTo(this);
        this.menuPrincipalU.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lAtrasMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> domingo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jLinea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane37;
    private javax.swing.JScrollPane jScrollPane38;
    private javax.swing.JScrollPane jScrollPane39;
    private javax.swing.JScrollPane jScrollPane69;
    private javax.swing.JScrollPane jScrollPane70;
    private javax.swing.JScrollPane jScrollPane71;
    private javax.swing.JScrollPane jScrollPane72;
    private javax.swing.JList<String> jueves;
    private javax.swing.JLabel lAtras;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JList<String> lunes;
    private javax.swing.JList<String> martes;
    private javax.swing.JList<String> miercoles;
    private javax.swing.JComboBox<String> plan;
    private javax.swing.JList<String> sabado;
    private javax.swing.JList<String> viernes;
    // End of variables declaration//GEN-END:variables
}
