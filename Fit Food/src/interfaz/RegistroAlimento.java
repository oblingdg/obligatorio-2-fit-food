package interfaz;

import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

public class RegistroAlimento extends javax.swing.JFrame {

    Sistema sistema;
    MenuPrincipalUsuario menuPrincipalU;
    MenuPrincipalProfesional menuPrincipalP;
    boolean esProf;
    
    public RegistroAlimento(Sistema sis, Object ventana, boolean esProf) {
        initComponents();
        this.sistema = sis;
        this.esProf = esProf;
        if(this.esProf){
            this.menuPrincipalP = (MenuPrincipalProfesional) ventana;
        }
        else{
            this.menuPrincipalU = (MenuPrincipalUsuario) ventana;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        btnIniciarSesion1 = new javax.swing.JButton();
        lNombre = new javax.swing.JLabel();
        lTipo = new javax.swing.JLabel();
        lNutrientes = new javax.swing.JLabel();
        tfNombre = new javax.swing.JTextField();
        comboTipo = new javax.swing.JComboBox<>();
        spinnerSodio = new javax.swing.JSpinner();
        spinnerCalorias = new javax.swing.JSpinner();
        spinnerCarbohidratos = new javax.swing.JSpinner();
        spinnerProteinas = new javax.swing.JSpinner();
        spinnerGrasas = new javax.swing.JSpinner();
        lSodio = new javax.swing.JLabel();
        lCalorias = new javax.swing.JLabel();
        lCarbohidratos = new javax.swing.JLabel();
        lProteinas = new javax.swing.JLabel();
        lGrasas = new javax.swing.JLabel();
        lPorcion = new javax.swing.JLabel();
        spinnerPorcion = new javax.swing.JSpinner();
        lAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        btnIniciarSesion1.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion1.setText("Ingresar");
        btnIniciarSesion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesion1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciarSesion1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 570, 150, 40));

        lNombre.setText("Nombre");
        jPanel1.add(lNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        lTipo.setText("Tipo");
        jPanel1.add(lTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, -1, -1));

        lNutrientes.setText("Nutrientes:");
        jPanel1.add(lNutrientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, -1, -1));
        jPanel1.add(tfNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 260, -1));

        comboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Derivados de la leche", "Carnes y huevo", "Legumbres y frutos secos", "Hortalizas y verduras", "Frutas", "Cereales" }));
        jPanel1.add(comboTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 240, 220, -1));

        spinnerSodio.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.1f));
        jPanel1.add(spinnerSodio, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 500, 60, 30));

        spinnerCalorias.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.1f));
        jPanel1.add(spinnerCalorias, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, 80, 30));

        spinnerCarbohidratos.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.1f));
        jPanel1.add(spinnerCarbohidratos, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 380, 60, 30));

        spinnerProteinas.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.1f));
        jPanel1.add(spinnerProteinas, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 420, 60, 30));

        spinnerGrasas.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.1f));
        jPanel1.add(spinnerGrasas, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 460, 60, 30));

        lSodio.setText("Sodio (g)");
        jPanel1.add(lSodio, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 500, -1, 30));

        lCalorias.setText("Calorias (kcal)");
        jPanel1.add(lCalorias, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 340, -1, 30));

        lCarbohidratos.setText("Carbohidratos (g)");
        jPanel1.add(lCarbohidratos, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 380, -1, 30));

        lProteinas.setText("Proteinas (g)");
        jPanel1.add(lProteinas, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 420, -1, 30));

        lGrasas.setText("Grasas (g)");
        jPanel1.add(lGrasas, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 460, -1, 30));

        lPorcion.setText("Porcion (g)");
        jPanel1.add(lPorcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 300, -1, -1));
        jPanel1.add(spinnerPorcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 300, 70, -1));

        lAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        lAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lAtrasMouseClicked(evt);
            }
        });
        jPanel1.add(lAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarSesion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesion1ActionPerformed
        if (this.tfNombre.getText().equals("") || (int)this.spinnerPorcion.getValue() == 0){
            JOptionPane.showMessageDialog(null, "Debe ingresar el nombre del alimento y la porcion", "Error al registrar alimento", 0);
        }
        else if(this.sistema.existeAlimento(this.lNombre.getText())){
            JOptionPane.showMessageDialog(null, "Ya existe un alimento con ese nombre.", "Error al registrar alimento", 0);
        }
        else{
            String nombre = this.tfNombre.getText();
            String tipo = this.comboTipo.getSelectedItem().toString();
            int porcion = Integer.parseInt(this.spinnerPorcion.getValue().toString());
            float[] nutrientes = new float[5];
            nutrientes[0] = Float.parseFloat(this.spinnerCalorias.getValue().toString());
            nutrientes[1] = Float.parseFloat(this.spinnerCarbohidratos.getValue().toString());
            nutrientes[2] = Float.parseFloat(this.spinnerProteinas.getValue().toString());
            nutrientes[3] = Float.parseFloat(this.spinnerGrasas.getValue().toString());
            nutrientes[4] = Float.parseFloat(this.spinnerSodio.getValue().toString());
            this.sistema.agregarAlimento(nombre, tipo, porcion, nutrientes);
            if(esProf){
                this.menuPrincipalP.setLocationRelativeTo(this);
                this.menuPrincipalP.setVisible(true);
            }
            else{
                this.menuPrincipalU.setLocationRelativeTo(this);
                this.menuPrincipalU.setVisible(true);
            }
            this.dispose();
        }
    }//GEN-LAST:event_btnIniciarSesion1ActionPerformed

    private void lAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lAtrasMouseClicked
        if(esProf){
                this.menuPrincipalP.setLocationRelativeTo(this);
                this.menuPrincipalP.setVisible(true);
            }
            else{
                this.menuPrincipalU.setLocationRelativeTo(this);
                this.menuPrincipalU.setVisible(true);
            }
        this.dispose();
    }//GEN-LAST:event_lAtrasMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciarSesion1;
    private javax.swing.JComboBox<String> comboTipo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lAtras;
    private javax.swing.JLabel lCalorias;
    private javax.swing.JLabel lCarbohidratos;
    private javax.swing.JLabel lGrasas;
    private javax.swing.JLabel lNombre;
    private javax.swing.JLabel lNutrientes;
    private javax.swing.JLabel lPorcion;
    private javax.swing.JLabel lProteinas;
    private javax.swing.JLabel lSodio;
    private javax.swing.JLabel lTipo;
    private javax.swing.JLabel lTitulo;
    private javax.swing.JSpinner spinnerCalorias;
    private javax.swing.JSpinner spinnerCarbohidratos;
    private javax.swing.JSpinner spinnerGrasas;
    private javax.swing.JSpinner spinnerPorcion;
    private javax.swing.JSpinner spinnerProteinas;
    private javax.swing.JSpinner spinnerSodio;
    private javax.swing.JTextField tfNombre;
    // End of variables declaration//GEN-END:variables
}
