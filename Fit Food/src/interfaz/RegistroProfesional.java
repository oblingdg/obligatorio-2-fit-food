package interfaz;

import java.awt.Image;
import dominio.Sistema;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;

public class RegistroProfesional extends javax.swing.JFrame {
    
    Sistema sistema;
    VentanaDeInicio ventanaInicio;
    boolean foto;
    ImageIcon icon;
    
    public RegistroProfesional(Sistema sis, VentanaDeInicio ventanaInicio) {
        initComponents();
        this.sistema = sis;
        this.ventanaInicio = ventanaInicio;
        this.icon = new ImageIcon(getClass().getResource("/imagenes/fotoPerfil.png"));
        this.lFoto.setIcon(icon);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lNombre = new javax.swing.JTextField();
        lApellido = new javax.swing.JTextField();
        lTitulo = new javax.swing.JTextField();
        comboPais = new javax.swing.JComboBox<>();
        btnImg = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        spinDiaG = new javax.swing.JComboBox<>();
        spinMesG = new javax.swing.JComboBox<>();
        spinAnioG = new javax.swing.JComboBox<>();
        spinAnioN = new javax.swing.JComboBox<>();
        spinMesN = new javax.swing.JComboBox<>();
        spinDiaN = new javax.swing.JComboBox<>();
        lFoto = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lTitulo1 = new javax.swing.JLabel();
        lSubtitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(500, 720));
        setPreferredSize(new java.awt.Dimension(480, 670));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setMaximumSize(new java.awt.Dimension(520, 670));
        jPanel1.setMinimumSize(new java.awt.Dimension(520, 670));
        jPanel1.setPreferredSize(new java.awt.Dimension(520, 670));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(lNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, 190, -1));
        jPanel1.add(lApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 170, 190, -1));
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 300, 150, -1));

        comboPais.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue" }));
        jPanel1.add(comboPais, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 420, 140, -1));

        btnImg.setBackground(new java.awt.Color(255, 255, 255));
        btnImg.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnImg.setForeground(new java.awt.Color(153, 0, 0));
        btnImg.setText("Seleccionar Imagen");
        btnImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImgActionPerformed(evt);
            }
        });
        jPanel1.add(btnImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 550, -1, -1));

        btnAceptar.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnAceptar.setForeground(new java.awt.Color(153, 0, 0));
        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 600, -1, -1));

        spinDiaG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        jPanel1.add(spinDiaG, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 350, -1, -1));

        spinMesG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        jPanel1.add(spinMesG, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 350, -1, -1));

        spinAnioG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900" }));
        jPanel1.add(spinAnioG, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 350, -1, -1));

        spinAnioN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900" }));
        jPanel1.add(spinAnioN, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 230, -1, -1));

        spinMesN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        jPanel1.add(spinMesN, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, -1, -1));

        spinDiaN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        spinDiaN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                spinDiaNActionPerformed(evt);
            }
        });
        jPanel1.add(spinDiaN, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 230, -1, -1));
        jPanel1.add(lFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 540, 50, 50));

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo1.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo1.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo1.setText("FIT FOOD");
        jPanel1.add(lTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        lSubtitulo.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        lSubtitulo.setForeground(new java.awt.Color(255, 255, 255));
        lSubtitulo.setText("Registro de Usuario");
        jPanel1.add(lSubtitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 70, -1, -1));

        jLabel1.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("País donde se graduó:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, -1, 30));

        jLabel3.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nombre:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, -1, -1));

        jLabel4.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Apellido:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jLabel5.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Fecha de Nacimiento: ");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, -1));

        jLabel6.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Fecha de graduación:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 350, -1, -1));

        jLabel16.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("DD/MM/AA");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        jLabel7.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Nombre de titulo:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        jLabel8.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Seleccione una imagen (opcional):");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, -1, 30));

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 520, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void spinDiaNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_spinDiaNActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_spinDiaNActionPerformed

    private void btnImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImgActionPerformed
        String aux = "";
        int resultado;
        JFileChooser file = new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("JPG y PNG", "jpg", "png");
        file.setAcceptAllFileFilterUsed(false);
        file.setFileFilter(filtro);
        resultado=file.showOpenDialog(null);
        if (resultado == JFileChooser.APPROVE_OPTION) {
            aux= file.getSelectedFile().getPath();
        }
        if(!aux.equalsIgnoreCase("")){
            this.icon = new ImageIcon(aux);
            this.icon = new ImageIcon(icon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
            lFoto.setIcon(icon);
        }
    }//GEN-LAST:event_btnImgActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if(this.lNombre.getText().equals("") || (this.lApellido.getText().equals("")) || (this.lTitulo.getText().equals(""))){
            JOptionPane.showMessageDialog(null, "Debe ingresar los datos nombre, apellido y titulo.", "Error al registrar profesional", 0);
        }
        else if(this.sistema.existeProfesional(this.lNombre.getText(), this.lApellido.getText())){
            JOptionPane.showMessageDialog(null, "Ya existe un profesional con ese nombre y apellido.", "Error al registrar profesional", 0);
        }
        else{
            String nombre = this.lNombre.getText();
            String apellido = this.lApellido.getText();
            String titulo = this.lTitulo.getText();
            int diaN = Integer.parseInt(this.spinDiaN.getSelectedItem().toString());
            int mesN = Integer.parseInt(this.spinMesN.getSelectedItem().toString());
            int anioN = Integer.parseInt(this.spinAnioN.getSelectedItem().toString());
            int diaG = Integer.parseInt(this.spinDiaG.getSelectedItem().toString());
            int mesG = Integer.parseInt(this.spinMesG.getSelectedItem().toString());
            int anioG = Integer.parseInt(this.spinAnioG.getSelectedItem().toString());
            String pais = this.comboPais.getSelectedItem().toString();
            
            this.sistema.agregarProfesional(nombre, apellido, titulo, diaN, mesN, anioN, diaG, mesG, anioG, pais, this.icon);
            this.ventanaInicio.setLocationRelativeTo(this);
            this.ventanaInicio.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        this.ventanaInicio.setLocationRelativeTo(this);
        this.ventanaInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLabel2MouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
        
    }//GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnImg;
    private javax.swing.JComboBox<String> comboPais;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField lApellido;
    private javax.swing.JLabel lFoto;
    private javax.swing.JTextField lNombre;
    private javax.swing.JLabel lSubtitulo;
    private javax.swing.JTextField lTitulo;
    private javax.swing.JLabel lTitulo1;
    private javax.swing.JComboBox<String> spinAnioG;
    private javax.swing.JComboBox<String> spinAnioN;
    private javax.swing.JComboBox<String> spinDiaG;
    private javax.swing.JComboBox<String> spinDiaN;
    private javax.swing.JComboBox<String> spinMesG;
    private javax.swing.JComboBox<String> spinMesN;
    // End of variables declaration//GEN-END:variables
}
