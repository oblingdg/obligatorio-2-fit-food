package interfaz;
import java.awt.Image;
import java.util.*;
import dominio.*;
import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class RegistroUsuario extends javax.swing.JFrame {

    Sistema sistema;
    VentanaDeInicio ventanaInicio;
    boolean foto;
    ImageIcon icon;
    
    public RegistroUsuario(Sistema sis, VentanaDeInicio ventanaInicio) {
        initComponents();
        this.sistema = sis;
        this.ventanaInicio = ventanaInicio;
        this.icon = new ImageIcon(getClass().getResource("/imagenes/fotoPerfil.png"));
        this.lFoto.setIcon(icon);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnAceptar = new javax.swing.JButton();
        jNacionalidad = new javax.swing.JTextField();
        cbFugivoro = new javax.swing.JCheckBox();
        jSeparator2 = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        lSubtitulo = new javax.swing.JLabel();
        jNombre = new javax.swing.JTextField();
        jApellidos = new javax.swing.JTextField();
        jDia = new javax.swing.JComboBox<>();
        jMes = new javax.swing.JComboBox<>();
        jAnio = new javax.swing.JComboBox();
        lFoto = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        cbVegano = new javax.swing.JCheckBox();
        cbOrganico = new javax.swing.JCheckBox();
        cbMacrobiotico = new javax.swing.JCheckBox();
        cbVegetariano = new javax.swing.JCheckBox();
        cbDiabetes = new javax.swing.JCheckBox();
        cbCeliaco = new javax.swing.JCheckBox();
        cbOtros = new javax.swing.JCheckBox();
        jOtra = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 102, 0));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(61, 91, 41));
        jPanel1.setMaximumSize(new java.awt.Dimension(520, 670));
        jPanel1.setMinimumSize(new java.awt.Dimension(520, 670));
        jPanel1.setPreferredSize(new java.awt.Dimension(520, 670));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnAceptar.setBackground(new java.awt.Color(255, 255, 255));
        btnAceptar.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnAceptar.setForeground(new java.awt.Color(153, 0, 0));
        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAceptar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 730, 210, 50));
        jPanel1.add(jNacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 300, 140, -1));

        cbFugivoro.setOpaque(false);
        cbFugivoro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                none(evt);
            }
        });
        jPanel1.add(cbFugivoro, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 740, -1, -1));

        jSeparator2.setBackground(new java.awt.Color(196, 57, 58));
        jSeparator2.setForeground(new java.awt.Color(196, 57, 58));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 45)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel1.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        lSubtitulo.setFont(new java.awt.Font("Serif", 0, 18)); // NOI18N
        lSubtitulo.setForeground(new java.awt.Color(255, 255, 255));
        lSubtitulo.setText("Registro de Usuario");
        jPanel1.add(lSubtitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 70, -1, -1));
        jPanel1.add(jNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 110, 140, -1));
        jPanel1.add(jApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 174, 140, -1));

        jDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        jPanel1.add(jDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, 60, -1));

        jMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        jMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                none(evt);
            }
        });
        jPanel1.add(jMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 230, 60, -1));

        jAnio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900" }));
        jPanel1.add(jAnio, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 230, 70, -1));

        lFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fotoPerfil.png"))); // NOI18N
        jPanel1.add(lFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 390, 50, 50));

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jButton3.setForeground(new java.awt.Color(204, 0, 0));
        jButton3.setText("Seleccionar Imagen");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, 170, 20));

        cbVegano.setOpaque(false);
        jPanel1.add(cbVegano, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 560, -1, 20));

        cbOrganico.setOpaque(false);
        jPanel1.add(cbOrganico, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 700, -1, 20));

        cbMacrobiotico.setOpaque(false);
        jPanel1.add(cbMacrobiotico, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 650, -1, 20));

        cbVegetariano.setOpaque(false);
        jPanel1.add(cbVegetariano, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 600, -1, 20));

        cbDiabetes.setOpaque(false);
        jPanel1.add(cbDiabetes, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 550, -1, -1));

        cbCeliaco.setOpaque(false);
        jPanel1.add(cbCeliaco, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 600, -1, -1));

        cbOtros.setOpaque(false);
        jPanel1.add(cbOtros, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 660, -1, -1));
        jPanel1.add(jOtra, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 660, 160, 30));

        jLabel1.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Preferencias y Restricciones:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, -1, -1));

        jLabel3.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nombre:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, -1, -1));

        jLabel4.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Apellido:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jLabel5.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Fecha de Nacimiento: ");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, -1));

        jLabel6.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nacionalidad:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        jLabel7.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Vegano");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 550, -1, -1));

        jLabel8.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Vegetariano");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 590, -1, -1));

        jLabel9.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Macrobiótico");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 640, -1, -1));

        jLabel10.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Orgánico");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 690, -1, -1));

        jLabel11.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Fugivoro");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 740, -1, -1));

        jLabel12.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Diabetes");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 550, -1, -1));

        jLabel13.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Celíaco");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 600, -1, -1));

        jLabel14.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Otros:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 660, -1, -1));

        jLabel16.setFont(new java.awt.Font("Serif", 0, 24)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("DD/MM/AA");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/back.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 70, 60));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 810));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if ((this.jNombre.getText().equals("")) || (this.jApellidos.getText().equals("")) || (this.jNacionalidad.getText().equals("")) || (this.cbOtros.isSelected() && this.jOtra.getText().equals("")) ){
            JOptionPane.showMessageDialog(null, "Debe ingresar los datos nombre, apellido y nacionalidad. Si selecciona otros en preferencia, complete el campo.", "Error al registrar usuario", 0);
        }
        else if(this.sistema.existeUsuario(this.jNombre.getText(), this.jApellidos.getText())){
            JOptionPane.showMessageDialog(null, "Ya existe un usuario con ese nombre y apellido.", "Error al registrar usuario", 0);
        }
        else{
            String nombre = this.jNombre.getText();
            String apellido = this.jApellidos.getText();
            int dia= Integer.parseInt(this.jDia.getSelectedItem().toString());
            int mes = Integer.parseInt(this.jMes.getSelectedItem().toString());
            int anio = Integer.parseInt(this.jAnio.getSelectedItem().toString());
            String nacionalidad = this.jNacionalidad.getText();
            String otros = "";
             boolean[] problemas = new boolean[8];
            //Checkbox seleccionados
            if (this.cbVegano.isSelected()){
                problemas[0] = true;
            }
            if(this.cbVegetariano.isSelected()){
                problemas[1] = true;
            }
            if(this.cbMacrobiotico.isSelected()){
                problemas[2] = true;
            }
            if(this.cbOrganico.isSelected()){
                problemas[3] = true;
            }
            if(this.cbFugivoro.isSelected()){
                problemas[4] = true;
            }
            if(this.cbDiabetes.isSelected()){
                problemas[5] = true;
            }
            if(this.cbCeliaco.isSelected()){
                problemas[6] = true;
            }
            if(this.cbOtros.isSelected()){
                problemas[7] = true;
                otros = this.jOtra.getText();
            }
            this.sistema.agregarUsuario(nombre, apellido, nacionalidad, dia, mes, anio, problemas, otros, this.icon);
            this.ventanaInicio.setLocationRelativeTo(this);
            this.ventanaInicio.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String aux = "";
        int resultado;
        JFileChooser file = new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("JPG y PNG", "jpg", "png");
        file.setAcceptAllFileFilterUsed(false);
        file.setFileFilter(filtro);
        resultado=file.showOpenDialog(null);
        if (resultado == JFileChooser.APPROVE_OPTION) {
            aux= file.getSelectedFile().getPath();
        }
        if(!aux.equalsIgnoreCase("")){
            this.icon = new ImageIcon(aux);
            this.icon = new ImageIcon(icon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
            lFoto.setIcon(icon);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void none(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_none
        // TODO add your handling code here:
    }//GEN-LAST:event_none

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        this.ventanaInicio.setLocationRelativeTo(this);
        this.ventanaInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLabel2MouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JCheckBox cbCeliaco;
    private javax.swing.JCheckBox cbDiabetes;
    private javax.swing.JCheckBox cbFugivoro;
    private javax.swing.JCheckBox cbMacrobiotico;
    private javax.swing.JCheckBox cbOrganico;
    private javax.swing.JCheckBox cbOtros;
    private javax.swing.JCheckBox cbVegano;
    private javax.swing.JCheckBox cbVegetariano;
    private javax.swing.JComboBox jAnio;
    private javax.swing.JTextField jApellidos;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jDia;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JComboBox<String> jMes;
    private javax.swing.JTextField jNacionalidad;
    private javax.swing.JTextField jNombre;
    private javax.swing.JTextField jOtra;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lFoto;
    private javax.swing.JLabel lSubtitulo;
    private javax.swing.JLabel lTitulo;
    // End of variables declaration//GEN-END:variables
}
