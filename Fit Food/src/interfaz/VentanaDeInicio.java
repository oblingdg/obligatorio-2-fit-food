package interfaz;
import dominio.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

public class VentanaDeInicio extends javax.swing.JFrame {
    
    Sistema sistema;
    
    public VentanaDeInicio(Sistema sistema) {
        this.sistema= sistema;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        btnIniciarSesion = new javax.swing.JButton();
        jLinea = new javax.swing.JSeparator();
        lTitulo = new javax.swing.JLabel();
        lDoIt = new javax.swing.JLabel();
        btnRegProfesional = new javax.swing.JButton();
        btnRegUsuario = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Fit Food");
        setBounds(new java.awt.Rectangle(100, 100, 520, 700));
        setMaximumSize(new java.awt.Dimension(520, 670));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(61, 91, 41));
        jPanel2.setMaximumSize(new java.awt.Dimension(520, 550));
        jPanel2.setMinimumSize(new java.awt.Dimension(520, 550));
        jPanel2.setPreferredSize(new java.awt.Dimension(520, 550));
        jPanel2.setRequestFocusEnabled(false);
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnIniciarSesion.setForeground(new java.awt.Color(196, 57, 58));
        btnIniciarSesion.setText("INICIAR SESION");
        btnIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesionActionPerformed(evt);
            }
        });
        jPanel2.add(btnIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 450, 170, -1));

        jLinea.setBackground(new java.awt.Color(196, 57, 58));
        jLinea.setForeground(new java.awt.Color(196, 57, 58));
        jLinea.setOpaque(true);
        jPanel2.add(jLinea, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 520, 5));

        lTitulo.setFont(new java.awt.Font("Arial", 1, 80)); // NOI18N
        lTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lTitulo.setText("FIT FOOD");
        jPanel2.add(lTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, -1, -1));

        lDoIt.setFont(new java.awt.Font("Gabriola", 2, 50)); // NOI18N
        lDoIt.setForeground(new java.awt.Color(255, 255, 255));
        lDoIt.setText("Do it!");
        jPanel2.add(lDoIt, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, 100, -1));

        btnRegProfesional.setForeground(new java.awt.Color(196, 57, 58));
        btnRegProfesional.setText("Registrar Profesional");
        btnRegProfesional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegProfesionalActionPerformed(evt);
            }
        });
        jPanel2.add(btnRegProfesional, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 540, 180, 30));

        btnRegUsuario.setForeground(new java.awt.Color(196, 57, 58));
        btnRegUsuario.setText("Registrar Usuario");
        btnRegUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegUsuarioActionPerformed(evt);
            }
        });
        jPanel2.add(btnRegUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 540, 160, 30));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 670));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesionActionPerformed
    if(this.sistema.getListaUsuarios().isEmpty() && this.sistema.getListaProfesionales().isEmpty()){
        JOptionPane.showMessageDialog(null, "Debe haber al menos 1 usuario o 1 profesional ingresado en el sistema.", "Error al iniciar sesion", 0);
    }
    else{
        this.setVisible(false);
        IniciarSesion inicioSesion = new IniciarSesion(this.sistema, this);
        inicioSesion.setLocationRelativeTo(this);
        inicioSesion.setVisible(true);
    }
    }//GEN-LAST:event_btnIniciarSesionActionPerformed

    private void btnRegUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegUsuarioActionPerformed
        this.setVisible(false);
        RegistroUsuario registroUser = new RegistroUsuario(this.sistema, this);
        registroUser.setLocationRelativeTo(this);
        registroUser.setVisible(true);
    }//GEN-LAST:event_btnRegUsuarioActionPerformed

    private void btnRegProfesionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegProfesionalActionPerformed
        this.setVisible(false);
        RegistroProfesional registroProf = new RegistroProfesional(this.sistema, this);
        registroProf.setLocationRelativeTo(this);
        registroProf.setVisible(true);
    }//GEN-LAST:event_btnRegProfesionalActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            FileOutputStream archivo= new FileOutputStream("persistencia.fitfood");
            BufferedOutputStream buf= new BufferedOutputStream(archivo);
            ObjectOutputStream obj = new ObjectOutputStream(buf);
            obj.writeObject(sistema);
            obj.flush();
            obj.close();
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_formWindowClosing

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciarSesion;
    private javax.swing.JButton btnRegProfesional;
    private javax.swing.JButton btnRegUsuario;
    private javax.swing.JSeparator jLinea;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lDoIt;
    private javax.swing.JLabel lTitulo;
    // End of variables declaration//GEN-END:variables
}
