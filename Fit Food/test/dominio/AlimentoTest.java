/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author DOMINIQUE
 */
public class AlimentoTest {
    
    public AlimentoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Alimento.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Alimento instance = new Alimento();
        String expResult = null;
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Alimento.
     */
     @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String aNombre = "Pascualina";
        Alimento instance = new Alimento();
        instance.setNombre(aNombre);
        assertEquals(aNombre,instance.getNombre());
    }

    /**
     * Test of getTipo method, of class Alimento.
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Alimento instance = new Alimento();
        String expResult = null;
        String result = instance.getTipo();
        assertEquals(expResult, result);
   }

    /**
     * Test of setTipo method, of class Alimento.
     */
    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        String aTipo = "";
        Alimento instance = new Alimento();
        instance.setTipo(aTipo);
        assertEquals(aTipo, instance.getTipo());
    }

    /**
     * Test of getPorcion method, of class Alimento.
     */
    @Test
    public void testGetPorcion() {
        System.out.println("getPorcion");
        Alimento instance = new Alimento();
        int expResult = 0;
        int result = instance.getPorcion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPorcion method, of class Alimento.
     */
    @Test
    public void testSetPorcion() {
        System.out.println("setPorcion");
        int aPorcion = 0;
        Alimento instance = new Alimento();
        instance.setPorcion(aPorcion);
        assertEquals(aPorcion,instance.getPorcion());
    }

    /**
     * Test of getNutrientes method, of class Alimento.
     */
    @Test
    public void testGetNutrientes() {
        System.out.println("getNutrientes");
        Alimento instance = new Alimento();
        float[] expResult = null;
        float[] result = instance.getNutrientes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNutrientes method, of class Alimento.
     */
    @Test
    public void testSetNutrientes() {
        System.out.println("setNutrientes");
        float[] aNutrientes = null;
        Alimento instance = new Alimento();
        instance.setNutrientes(aNutrientes);
        assertEquals(aNutrientes, instance.getNutrientes());
    }

    /**
     * Test of equals method, of class Alimento.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Alimento instance = new Alimento();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        
    }
    
}
