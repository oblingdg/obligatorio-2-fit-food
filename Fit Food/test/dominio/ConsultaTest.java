/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DOMINIQUE
 */
public class ConsultaTest {
    
    public ConsultaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUser method, of class Consulta.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        Consulta instance = new Consulta();
        Usuario expResult = null;
        Usuario result = instance.getUser();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUser method, of class Consulta.
     */
    @Test
    public void testSetUser() {
        System.out.println("setUser");
        Usuario user = null;
        Consulta instance = new Consulta();
        instance.setUser(user);
        assertEquals(user, instance.getUser());
    }

    /**
     * Test of getPregunta method, of class Consulta.
     */
    @Test
    public void testGetPregunta() {
        System.out.println("getPregunta");
        Consulta instance = new Consulta();
        String expResult = null;
        String result = instance.getPregunta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPregunta method, of class Consulta.
     */
    @Test
    public void testSetPregunta() {
        System.out.println("setPregunta");
        String pregunta = null;
        Consulta instance = new Consulta();
        instance.setPregunta(pregunta);
        assertEquals(pregunta, instance.getPregunta());
    }

    /**
     * Test of getRespuesta method, of class Consulta.
     */
    @Test
    public void testGetRespuesta() {
        System.out.println("getRespuesta");
        Consulta instance = new Consulta();
        String expResult = null;
        String result = instance.getRespuesta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRespuesta method, of class Consulta.
     */
    @Test
    public void testSetRespuesta() {
        System.out.println("setRespuesta");
        String respuesta = null;
        Consulta instance = new Consulta();
        instance.setRespuesta(respuesta);
        assertEquals(respuesta, instance.getRespuesta());
    }

    /**
     * Test of getPro method, of class Consulta.
     */
    @Test
    public void testGetPro() {
        System.out.println("getPro");
        Consulta instance = new Consulta();
        Profesional expResult = null;
        Profesional result = instance.getPro();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPro method, of class Consulta.
     */
    @Test
    public void testSetPro() {
        System.out.println("setPro");
        Profesional pro = null;
        Consulta instance = new Consulta();
        instance.setPro(pro);
        assertEquals(pro, instance.getPro());
    }

    /**
     * Test of enviarRespuesta method, of class Consulta.
     */
    @Test
    public void testEnviarRespuesta() {
        System.out.println("enviarRespuesta");
        Usuario usu = new Usuario();
        Consulta instance = new Consulta();
        instance.setUser(usu);
        instance.setRespuesta("respuesta");
        instance.enviarRespuesta();
        assertEquals("respuesta",usu.getRespuestas().get(0).getRespuesta());
    }
    
}
