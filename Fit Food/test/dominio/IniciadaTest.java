/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DOMINIQUE
 */
public class IniciadaTest {
    
    public IniciadaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUsuario method, of class Iniciada.
     */
    @Test
    public void testGetUsuario() {
        System.out.println("getUsuario");
        Iniciada instance = new Iniciada();
        Usuario expResult = null;
        Usuario result = instance.getUsuario();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUsuario method, of class Iniciada.
     */
    @Test
    public void testSetUsuario() {
        System.out.println("setUsuario");
        Usuario usu = null;
        Iniciada instance = new Iniciada();
        instance.setUsuario(usu);
        assertEquals(usu, instance.getUsuario());
    }

    /**
     * Test of getProfesional method, of class Iniciada.
     */
    @Test
    public void testGetProfesional() {
        System.out.println("getProfesional");
        Iniciada instance = new Iniciada();
        Profesional expResult = null;
        Profesional result = instance.getProfesional();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProfesional method, of class Iniciada.
     */
    @Test
    public void testSetProfesional() {
        System.out.println("setProfesional");
        Profesional prof = null;
        Iniciada instance = new Iniciada();
        instance.setProfesional(prof);
        assertEquals(prof, instance.getProfesional());
    }

    /**
     * Test of getFoto method, of class Iniciada.
     */
    @Test
    public void testGetFoto() {
        System.out.println("getFoto");
        Iniciada instance = new Iniciada();
        ImageIcon expResult = null;
        ImageIcon result = instance.getFoto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFoto method, of class Iniciada.
     */
    @Test
    public void testSetFoto() {
        System.out.println("setFoto");
        ImageIcon foto = null;
        Iniciada instance = new Iniciada();
        instance.setFoto(foto);
        assertEquals(foto,instance.getFoto());
    }

    /**
     * Test of isEsPro method, of class Iniciada.
     */
    @Test
    public void testIsEsPro() {
        System.out.println("isEsPro");
        Iniciada instance = new Iniciada();
        boolean expResult = false;
        boolean result = instance.isEsPro();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setEsPro method, of class Iniciada.
     */
    @Test
    public void testSetEsPro() {
        System.out.println("setEsPro");
        boolean esPro = false;
        Iniciada instance = new Iniciada();
        instance.setEsPro(esPro);
        assertEquals(esPro, instance.isEsPro());
    }
    
}
