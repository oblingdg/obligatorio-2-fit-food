/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DOMINIQUE
 */
public class ProfesionalTest {

    public ProfesionalTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Profesional.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Profesional instance = new Profesional();
        String expResult = null;
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Profesional.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre = null;
        Profesional instance = new Profesional();
        instance.setNombre(nombre);
        assertEquals(nombre, instance.getNombre());
    }

    /**
     * Test of getApellido method, of class Profesional.
     */
    @Test
    public void testGetApellido() {
        System.out.println("getApellido");
        Profesional instance = new Profesional();
        String expResult = null;
        String result = instance.getApellido();
        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido method, of class Profesional.
     */
    @Test
    public void testSetApellido() {
        System.out.println("setApellido");
        String apellido = null;
        Profesional instance = new Profesional();
        instance.setApellido(apellido);
        assertEquals(apellido, instance.getApellido());
    }

    /**
     * Test of getTitulo method, of class Profesional.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Profesional instance = new Profesional();
        String expResult = null;
        String result = instance.getTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTitulo method, of class Profesional.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String titulo = null;
        Profesional instance = new Profesional();
        instance.setTitulo(titulo);
        assertEquals(titulo, instance.getTitulo());
    }

    /**
     * Test of getFecha_D method, of class Profesional.
     */
    @Test
    public void testGetFecha_D() {
        System.out.println("getFecha_D");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFecha_D();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_D method, of class Profesional.
     */
    @Test
    public void testSetFecha_D() {
        System.out.println("setFecha_D");
        int fecha_D = 0;
        Profesional instance = new Profesional();
        instance.setFecha_D(fecha_D);
        assertEquals(fecha_D, instance.getFecha_D());
    }

    /**
     * Test of getFecha_M method, of class Profesional.
     */
    @Test
    public void testGetFecha_M() {
        System.out.println("getFecha_M");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFecha_M();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_M method, of class Profesional.
     */
    @Test
    public void testSetFecha_M() {
        System.out.println("setFecha_M");
        int fecha_M = 0;
        Profesional instance = new Profesional();
        instance.setFecha_M(fecha_M);
        assertEquals(fecha_M, instance.getFecha_M());
    }

    /**
     * Test of getFecha_A method, of class Profesional.
     */
    @Test
    public void testGetFecha_A() {
        System.out.println("getFecha_A");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFecha_A();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_A method, of class Profesional.
     */
    @Test
    public void testSetFecha_A() {
        System.out.println("setFecha_A");
        int fecha_A = 0;
        Profesional instance = new Profesional();
        instance.setFecha_A(fecha_A);
         assertEquals(fecha_A, instance.getFecha_A());
    }

    /**
     * Test of getFechaG_D method, of class Profesional.
     */
    @Test
    public void testGetFechaG_D() {
        System.out.println("getFechaG_D");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFechaG_D();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaG_D method, of class Profesional.
     */
    @Test
    public void testSetFechaG_D() {
        System.out.println("setFechaG_D");
        int fechaG_D = 0;
        Profesional instance = new Profesional();
        instance.setFechaG_D(fechaG_D);
        assertEquals(fechaG_D,instance.getFechaG_D());
    }

    /**
     * Test of getFechaG_M method, of class Profesional.
     */
    @Test
    public void testGetFechaG_M() {
        System.out.println("getFechaG_M");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFechaG_M();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaG_M method, of class Profesional.
     */
    @Test
    public void testSetFechaG_M() {
        System.out.println("setFechaG_M");
        int fechaG_M = 0;
        Profesional instance = new Profesional();
        instance.setFechaG_M(fechaG_M);
        assertEquals(fechaG_M,instance.getFechaG_M());
    }

    /**
     * Test of getFechaG_A method, of class Profesional.
     */
    @Test
    public void testGetFechaG_A() {
        System.out.println("getFechaG_A");
        Profesional instance = new Profesional();
        int expResult = 0;
        int result = instance.getFechaG_A();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaG_A method, of class Profesional.
     */
    @Test
    public void testSetFechaG_A() {
        System.out.println("setFechaG_A");
        int fechaG_A = 0;
        Profesional instance = new Profesional();
        instance.setFechaG_A(fechaG_A);
        assertEquals(fechaG_A,instance.getFechaG_A());
    }

    /**
     * Test of getPais_T method, of class Profesional.
     */
    @Test
    public void testGetPais_T() {
        System.out.println("getPais_T");
        Profesional instance = new Profesional();
        String expResult = null;
        String result = instance.getPais_T();
        assertEquals(expResult, result);    }

    /**
     * Test of setPais_T method, of class Profesional.
     */
    @Test
    public void testSetPais_T() {
        System.out.println("setPais_T");
        String pais_T = null;
        Profesional instance = new Profesional();
        instance.setPais_T(pais_T);
        assertEquals(pais_T,instance.getTitulo());
    }

    /**
     * Test of getFoto method, of class Profesional.
     */
    @Test
    public void testGetFoto() {
        System.out.println("getFoto");
        Profesional instance = new Profesional();
        ImageIcon expResult = null;
        ImageIcon result = instance.getFoto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFoto method, of class Profesional.
     */
    @Test
    public void testSetFoto() {
        System.out.println("setFoto");
        ImageIcon foto = null;
        Profesional instance = new Profesional();
        instance.setFoto(foto);
        assertEquals(foto,instance.getFoto());
    }

    /**
     * Test of equals method, of class Profesional.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Profesional instance = new Profesional();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        
    }

}
