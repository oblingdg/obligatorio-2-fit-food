/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DOMINIQUE
 */
public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaUsuarios method, of class Sistema.
     */
    @Test
    public void testGetListaUsuarios() {
        System.out.println("getListaUsuarios");
        Sistema instance = new Sistema();
        ArrayList<Usuario> expResult = new ArrayList();
        ArrayList<Usuario> result = instance.getListaUsuarios();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaUsuarios method, of class Sistema.
     */
    @Test
    public void testSetListaUsuarios() {
        System.out.println("setListaUsuarios");
        ArrayList<Usuario> lista = null;
        Sistema instance = new Sistema();
        instance.setListaUsuarios(lista);
        assertEquals(lista,instance.getListaUsuarios());
    }

    /**
     * Test of getListaProfesionales method, of class Sistema.
     */
    @Test
    public void testGetListaProfesionales() {
        System.out.println("getListaProfesionales");
        Sistema instance = new Sistema();
        ArrayList<Profesional> expResult = new ArrayList();
        ArrayList<Profesional> result = instance.getListaProfesionales();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaProfesionales method, of class Sistema.
     */
    @Test
    public void testSetListaProfesionales() {
        System.out.println("setListaProfesionales");
        ArrayList<Profesional> lista = null;
        Sistema instance = new Sistema();
        instance.setListaProfesionales(lista);
        assertEquals(lista,instance.getListaProfesionales());
    }

    /**
     * Test of getListAlimentos method, of class Sistema.
     */
    @Test
    public void testGetListAlimentos() {
        System.out.println("getListAlimentos");
        Sistema instance = new Sistema();
        ArrayList<Alimento> expResult = new ArrayList();
        ArrayList<Alimento> result = instance.getListAlimentos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaAlimentos method, of class Sistema.
     */
    @Test
    public void testSetListaAlimentos() {
        System.out.println("setListaAlimentos");
        ArrayList<Alimento> lista = null;
        Sistema instance = new Sistema();
        instance.setListaAlimentos(lista);
        assertEquals(lista,instance.getListAlimentos());
    }

    /**
     * Test of getListaPlanes method, of class Sistema.
     */
    @Test
    public void testGetListaPlanes() {
        System.out.println("getListaPlanes");
        Sistema instance = new Sistema();
        ArrayList<Usuario> expResult = new ArrayList();
        ArrayList<Usuario> result = instance.getListaPlanes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaPlanes method, of class Sistema.
     */
    @Test
    public void testSetListaPlanes() {
        System.out.println("setListaPlanes");
        ArrayList<Usuario> listaPlanes = null;
        Sistema instance = new Sistema();
        instance.setListaPlanes(listaPlanes);
        assertEquals(listaPlanes, instance.getListaPlanes());
    }

    /**
     * Test of getListaConsultas method, of class Sistema.
     */
    @Test
    public void testGetListaConsultas() {
        System.out.println("getListaConsultas");
        Sistema instance = new Sistema();
        ArrayList<Consulta> expResult = new ArrayList();
        ArrayList<Consulta> result = instance.getListaConsultas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaConsultas method, of class Sistema.
     */
    @Test
    public void testSetListaConsultas() {
        System.out.println("setListaConsultas");
        ArrayList<Consulta> consultas = null;
        Sistema instance = new Sistema();
        instance.setListaConsultas(consultas);
        assertEquals(consultas,instance.getListaConsultas());
    }

    /**
     * Test of agregarUsuario method, of class Sistema.
     */
    @Test
    public void testAgregarUsuario() {
        System.out.println("agregarUsuario");
        String nombre = null;
        String apellido = null;
        String nacionalidad = null;
        int dia = 0;
        int mes = 0;
        int anio = 0;
        boolean[] problemas = null;
        String otros = null;
        ImageIcon foto = null;
        Sistema instance = new Sistema();
        instance.agregarUsuario(nombre, apellido, nacionalidad, dia, mes, anio, problemas, otros, foto);
        Usuario u = new Usuario();
        u.setNombre(nombre);
        u.setApellido(apellido);
        u.setNacionalidad(nacionalidad);
        u.setFecha_D(dia);
        u.setFecha_M(mes);
        u.setFecha_A(anio);
        u.setListaPreferencias(problemas);
        u.setOtrasPreferencias(otros);
        u.setFoto(foto);
        assertEquals(u,instance.getListaUsuarios().get(0));
    }

    /**
     * Test of existeUsuario method, of class Sistema.
     */
    @Test
    public void testExisteUsuario() {
        System.out.println("existeUsuario");
        String nombre = null;
        String apellido = null;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.existeUsuario(nombre, apellido);
        assertEquals(expResult, result);
    }

  
    /**
     * Test of agregarProfesional method, of class Sistema.
     */
    @Test
    public void testAgregarProfesional() {
        System.out.println("agregarProfesional");
        String nombre = null;
        String apellido = null;
        String titulo = null;
        int diaN = 0;
        int mesN = 0;
        int anioN = 0;
        int diaG = 0;
        int mesG = 0;
        int anioG = 0;
        String pais = null;
        ImageIcon foto = null;
        Sistema instance = new Sistema();
        instance.agregarProfesional(nombre, apellido, titulo, diaN, mesN, anioN, diaG, mesG, anioG, pais, foto);
        Profesional p = new Profesional();
        p.setNombre(nombre);
        p.setApellido(apellido);
        p.setTitulo(titulo);
        p.setFecha_D(diaN);
        p.setFecha_M(mesN);
        p.setFecha_A(anioN);
        p.setFechaG_D(diaG);
        p.setFechaG_M(mesG);
        p.setFechaG_A(anioG);
        p.setPais_T(pais);
        p.setFoto(foto);
        assertEquals(p,instance.getListaProfesionales().get(0));
    }

    /**
     * Test of existeProfesional method, of class Sistema.
     */
    @Test
    public void testExisteProfesional() {
        System.out.println("existeProfesional");
        String nombre = "";
        String apellido = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.existeProfesional(nombre, apellido);
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarAlimento method, of class Sistema.
     */
    @Test
    public void testAgregarAlimento() {
        System.out.println("agregarAlimento");
        String nombre = "";
        String tipo = "";
        int porcion = 0;
        float[] nutrientes = null;
        Sistema instance = new Sistema();
        instance.agregarAlimento(nombre, tipo, porcion, nutrientes);
        Alimento nuevo = new Alimento();
        nuevo.setNombre(nombre);
        nuevo.setNutrientes(nutrientes);
        nuevo.setPorcion(porcion);
        nuevo.setTipo(tipo);
        assertEquals(nuevo,instance.getListAlimentos().get(0));
        
    }

    /**
     * Test of getAlimento method, of class Sistema.
     */
    @Test
    public void testGetAlimento() {
        System.out.println("getAlimento");
        String s = "";
        Sistema instance = new Sistema();
        Alimento expResult = null;
        Alimento result = instance.getAlimento(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlimento method, of class Sistema.
     */
    @Test
    public void testExisteAlimento() {
        System.out.println("existeAlimento");
        String s = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.existeAlimento(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarConsulta method, of class Sistema.
     */
    @Test
    public void testAgregarConsulta() {
        System.out.println("agregarConsulta");
        Consulta consu = null;
        Sistema instance = new Sistema();
        instance.agregarConsulta(consu);
        assertEquals(consu,instance.getListaConsultas().get(0));
    }

    /**
     * Test of eliminarConsulta method, of class Sistema.
     */
    @Test
    public void testEliminarConsulta() {
        System.out.println("eliminarConsulta");
        Consulta consulta = null;
        Sistema instance = new Sistema();
        instance.eliminarConsulta(consulta);
        assertEquals(new ArrayList(),instance.getListaConsultas());
    }

    /**
     * Test of deStringAalimentos method, of class Sistema.
     */
    @Test
    public void testDeStringAalimentos() {
        System.out.println("deStringAalimentos");
        ArrayList<String> alimento = new ArrayList();
        Sistema instance = new Sistema();
        ArrayList<Alimento> expResult = new ArrayList();
        ArrayList<Alimento> result = instance.deStringAalimentos(alimento);
        assertEquals(expResult, result);
    }

    /**
     * Test of deListaArrayList method, of class Sistema.
     */
    @Test
    public void testDeListaArrayList() {
        System.out.println("deListaArrayList");
         List<String> lista = new ArrayList();
        Sistema instance = new Sistema();
        ArrayList<String> expResult = new ArrayList();
        ArrayList<String> result = instance.deListaArrayList(lista);
        assertEquals(expResult, result);
    }    

    /**
     * Test of deAlimentoAString method, of class Sistema.
     */
    @Test
    public void testDeAlimentoAString() {
        System.out.println("deAlimentoAString");
        ArrayList<Alimento> array = new ArrayList();
        Sistema instance = new Sistema();
        String[] expResult = new String[0];
        String[] result = instance.deAlimentoAString(array);
        assertArrayEquals(expResult, result);
    }
    
}
