/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DOMINIQUE
 */
public class UsuarioTest {
    
    public UsuarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Usuario.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Usuario instance = new Usuario();
        String expResult = null;
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Usuario.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre =null;
        Usuario instance = new Usuario();
        instance.setNombre(nombre);
        assertEquals(nombre,instance.getNombre());
    }

    /**
     * Test of getApellido method, of class Usuario.
     */
    @Test
    public void testGetApellido() {
        System.out.println("getApellido");
        Usuario instance = new Usuario();
        String expResult = null;
        String result = instance.getApellido();
        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido method, of class Usuario.
     */
    @Test
    public void testSetApellido() {
        System.out.println("setApellido");
        String apellido = null;
        Usuario instance = new Usuario();
        instance.setApellido(apellido);
        assertEquals(apellido,instance.getApellido());
    }

    /**
     * Test of getFecha_D method, of class Usuario.
     */
    @Test
    public void testGetFecha_D() {
        System.out.println("getFecha_D");
        Usuario instance = new Usuario();
        int expResult = 0;
        int result = instance.getFecha_D();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_D method, of class Usuario.
     */
    @Test
    public void testSetFecha_D() {
        System.out.println("setFecha_D");
        int fecha_D = 0;
        Usuario instance = new Usuario();
        instance.setFecha_D(fecha_D);
        assertEquals(fecha_D,instance.getFecha_D());
    }

    /**
     * Test of getFecha_M method, of class Usuario.
     */
    @Test
    public void testGetFecha_M() {
        System.out.println("getFecha_M");
        Usuario instance = new Usuario();
        int expResult = 0;
        int result = instance.getFecha_M();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_M method, of class Usuario.
     */
    @Test
    public void testSetFecha_M() {
        System.out.println("setFecha_M");
        int fecha_M = 0;
        Usuario instance = new Usuario();
        instance.setFecha_M(fecha_M);
        assertEquals(fecha_M,instance.getFecha_M());
    }

    /**
     * Test of getFecha_A method, of class Usuario.
     */
    @Test
    public void testGetFecha_A() {
        System.out.println("getFecha_A");
        Usuario instance = new Usuario();
        int expResult = 0;
        int result = instance.getFecha_A();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha_A method, of class Usuario.
     */
    @Test
    public void testSetFecha_A() {
        System.out.println("setFecha_A");
        int fecha_A = 0;
        Usuario instance = new Usuario();
        instance.setFecha_A(fecha_A);
        assertEquals(fecha_A,instance.getFecha_A());
    }

    /**
     * Test of getNacionalidad method, of class Usuario.
     */
    @Test
    public void testGetNacionalidad() {
        System.out.println("getNacionalidad");
        Usuario instance = new Usuario();
        String expResult = null;
        String result = instance.getNacionalidad();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNacionalidad method, of class Usuario.
     */
    @Test
    public void testSetNacionalidad() {
        System.out.println("setNacionalidad");
        String nacionalidad = null;
        Usuario instance = new Usuario();
        instance.setNacionalidad(nacionalidad);
        assertEquals(nacionalidad,instance.getNacionalidad());
    }

    /**
     * Test of getListaPreferencias method, of class Usuario.
     */
    @Test
    public void testGetListaPreferencias() {
        System.out.println("getListaPreferencias");
        Usuario instance = new Usuario();
        boolean[] expResult = null;
        boolean[] result = instance.getListaPreferencias();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of setListaPreferencias method, of class Usuario.
     */
    @Test
    public void testSetListaPreferencias() {
        System.out.println("setListaPreferencias");
        boolean[] listaPreferencias = null;
        Usuario instance = new Usuario();
        instance.setListaPreferencias(listaPreferencias);
        assertEquals(listaPreferencias, instance.getListaPreferencias());
    }

    /**
     * Test of getOtrasPreferencias method, of class Usuario.
     */
    @Test
    public void testGetOtrasPreferencias() {
        System.out.println("getOtrasPreferencias");
        Usuario instance = new Usuario();
        String expResult = null;
        String result = instance.getOtrasPreferencias();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOtrasPreferencias method, of class Usuario.
     */
    @Test
    public void testSetOtrasPreferencias() {
        System.out.println("setOtrasPreferencias");
        String otrasPreferencias = null;
        Usuario instance = new Usuario();
        instance.setOtrasPreferencias(otrasPreferencias);
        assertEquals(otrasPreferencias,instance.getOtrasPreferencias());
    }

    /**
     * Test of getFoto method, of class Usuario.
     */
    @Test
    public void testGetFoto() {
        System.out.println("getFoto");
        Usuario instance = new Usuario();
        ImageIcon expResult = null;
        ImageIcon result = instance.getFoto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFoto method, of class Usuario.
     */
    @Test
    public void testSetFoto() {
        System.out.println("setFoto");
        ImageIcon foto = null;
        Usuario instance = new Usuario();
        instance.setFoto(foto);
        assertEquals(foto,instance.getFoto());
    }

    /**
     * Test of getAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testGetAlimentosIngeridos() {
        System.out.println("getAlimentosIngeridos");
        Usuario instance = new Usuario();
        ArrayList<Alimento> expResult = new ArrayList();
        ArrayList<Alimento> result = instance.getAlimentosIngeridos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testSetAlimentosIngeridos() {
        System.out.println("setAlimentosIngeridos");
        ArrayList<Alimento> alimentosIngeridos = null;
        Usuario instance = new Usuario();
        instance.setAlimentosIngeridos(alimentosIngeridos);
        assertEquals(alimentosIngeridos,instance.getAlimentosIngeridos());
    }

    /**
     * Test of getRespuestas method, of class Usuario.
     */
    @Test
    public void testGetRespuestas() {
        System.out.println("getRespuestas");
        Usuario instance = new Usuario();
        ArrayList<Consulta> expResult = new ArrayList();
        ArrayList<Consulta> result = instance.getRespuestas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRespuestas method, of class Usuario.
     */
    @Test
    public void testSetRespuestas() {
        System.out.println("setRespuestas");
        ArrayList<Consulta> respuestas = null;
        Usuario instance = new Usuario();
        instance.setRespuestas(respuestas);
        assertEquals(respuestas,instance.getRespuestas());
    }

    /**
     * Test of getPlanes method, of class Usuario.
     */
    @Test
    public void testGetPlanes() {
        System.out.println("getPlanes");
        Usuario instance = new Usuario();
        ArrayList expResult = new ArrayList();
        ArrayList result = instance.getPlanes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPlanes method, of class Usuario.
     */
    @Test
    public void testSetPlanes() {
        System.out.println("setPlanes");
        ArrayList planes = null;
        Usuario instance = new Usuario();
        instance.setPlanes(planes);
        assertEquals(planes,instance.getPlanes());
    }

    /**
     * Test of agregarRespuesta method, of class Usuario.
     */
    @Test
    public void testAgregarRespuesta() {
        System.out.println("agregarRespuesta");
        Consulta consulta = new Consulta();
        Usuario instance = new Usuario();
        instance.agregarRespuesta(consulta);
        assertEquals(consulta,instance.getRespuestas().get(0));
    }

    /**
     * Test of agregarPlan method, of class Usuario.
     */
    @Test
    public void testAgregarPlan() {
        System.out.println("agregarPlan");
        ArrayList[] alimentos = null;
        Usuario instance = new Usuario();
        instance.agregarPlan(alimentos);
        assertArrayEquals(alimentos, instance.getPlanes().get(0));
    }

    /**
     * Test of equals method, of class Usuario.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Usuario instance = new Usuario();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
